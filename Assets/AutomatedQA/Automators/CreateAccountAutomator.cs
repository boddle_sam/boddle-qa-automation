﻿using GeneratedAutomationTests;
using System;
using System.Collections;
using Unity.AutomatedQA;
using Unity.AutomatedQA.Listeners;
using UnityEngine;
using UnityEngine.EventSystems;

[Serializable]
public class CreateAccountAutomatorConfig : AutomatorConfig<CreateAccountAutomator>
{
    public string name;
    public string lastInitial;
    public bool customUsername;
    public string username;
    public string password;
}

public class CreateAccountAutomator : Automator<CreateAccountAutomatorConfig>
{
    public override void Init(CreateAccountAutomatorConfig config)
    {
        base.Init(config);

        Debug.Log("[Boddle Bot] Automated");
    }
    public override void BeginAutomation()
    {
        base.BeginAutomation();
        Debug.Log("[Boddle Bot] Automated BEGIN");

        StartCoroutine(CreateAccount());
    }

    IEnumerator CreateAccount()
    {
        Debug.Log("[Boddle Bot] Starting Create account automation!");
        yield return new WaitForSeconds(2);

        Debug.Log("[Boddle Bot] Clicc ID");
        yield return Driver.Perform.Click("#CreateNewAccountButton");
        Debug.Log("[Boddle Bot] Clicked");

        Debug.Log("[Boddle Bot] Typing " + config.name);
        yield return Driver.Perform.SendKeys("#FirstName", config.name);
        Debug.Log("[Boddle Bot] Complete");

        EndAutomation();
    }

    public override void EndAutomation()
    {
        base.EndAutomation();
        Debug.Log("[Boddle Bot] Automated END");
    }
}