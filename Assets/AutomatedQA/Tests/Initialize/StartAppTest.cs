﻿using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;
using Unity.AutomatedQA;
using Unity.CloudTesting;
using GeneratedAutomationTests;
using NUnit.Framework;

namespace Boddle.Test.Application
{
    public class StartAppTest : AutomatedQATestsBase
    {

        [UnityTest]
        [CloudTest]
        public IEnumerator CanStartAppController()
        {
            float timer = Time.time; 
            GameObject coreStateMachineObj = null;
            yield return WaitFor(() => (coreStateMachineObj = GameObject.Find("[Core Game State Manager]")) != null);

            Assert.IsTrue(coreStateMachineObj, "Core Game State Manager Started");
        }

        // Steps defined by recording.
        protected override void SetUpTestClass()
        {
        }

        // Initialize test run data.
        protected override void SetUpTestRun()
        {
            Test.entryScene = "UIScene";
            Test.recordedAspectRatio = new Vector2(0, 0);
            Test.recordedResolution = new Vector2(0, 0);
        }
    }
}
