﻿using Boddle.App;
using Boddle.Data;
using Boddle.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Boddle.Providers
{
    /// <summary>
    /// Replace with comments...
    /// </summary>
    public static class GlobalDataProvider
    {
        // ***********************************************************
        // ********************* STATIC DATA MODELS ******************
        // ***********************************************************
        public static BoddleBotsModel BoddleBotsModel { get; private set; }

        // ***********************************************************
        // ********************** LOCAL DATA MODELS ******************
        // ***********************************************************

        private static HashSet<IDataModel> _Models = new HashSet<IDataModel>();

        public static void Initialize()
        {
            _Models = null;
            _Models = new HashSet<IDataModel>();
            // Static Data
            BoddleBotsModel = GenerateDataModel<BoddleBotsModel>("BoddleBotsModel");

            // Local Data

            // Initialize all data models
            foreach(IDataModel dataModel in _Models)
            {
                dataModel.InitializeModel();
            }
        }

        public static void PreLoadData()
        {
            foreach (IDataModel dataModel in _Models)
            {
                if (dataModel.LoadType != LoadType.PreLoad)
                    continue;
                if (dataModel.IsLoaded)
                    continue;

                IDataModel tempModel = dataModel;
                tempModel.LoadModel();
            }
        }
        
        public static void PostLoadData()
        {
            foreach (IDataModel dataModel in _Models)
            {
                if (dataModel.LoadType != LoadType.PostLoad)
                    continue;
                if (dataModel.IsLoaded)
                    continue;

                IDataModel tempModel = dataModel;
                tempModel.LoadModel();
            }
        }

        public static IDataModel[] GetModels()
        {
            return _Models.ToArray();
        }

        public static DataModelType GetModel<DataModelType>(Type type) where DataModelType : IDataModel
        {
            foreach (IDataModel dataModel in _Models)
            {
                if (dataModel.GetType().IsAssignableFrom(type) || dataModel.GetType().Equals(type))
                    return (DataModelType)dataModel;
            }

            return default(DataModelType);
        }

        public static DataModelType GenerateDataModel<DataModelType>(string name) where DataModelType : ScriptableObject, IDataModel
        {
            // Check if we have a preset set in the preferences
            DataModelType result = Resources.Load<DataModelType>(name);
            
            // Check if it exists
            if (result == null)
            {
                foreach (IDataModel dataModel in _Models)
                {
                    if (dataModel is DataModelType || dataModel.GetType().IsAssignableFrom(typeof(DataModelType)))
                        return (DataModelType)dataModel;
                }
            }

            // Use an empty new instance
            if (result == null)
            {
                result = ScriptableObject.CreateInstance<DataModelType>();
                // Add the model to the internal list of models
                _Models.Add(result);
                return result;
            }

            return result;
        }

        public static void RemoveDataModel(IDataModel dataModel) 
        {
            if (_Models.Contains(dataModel)) _Models.Remove(dataModel);
        }
        public static string GenerateLogDump()
        {
            string dump = "";
            foreach (IDataModel dataModel in _Models)
            {
                if (dataModel == null)
                    continue;
                try
                {
                    dump += dataModel.GetLogDump();
                }
                catch { }
            }
            return dump;
        }
    }
}