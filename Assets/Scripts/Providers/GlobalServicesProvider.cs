﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Boddle.Services;
using UnityEngine;
using System.Linq;
using Boddle.App;
using static Boddle.Services.BotService;

namespace Boddle.Providers
{
    public static class GlobalServicesProvider
    {
        public static BotService BotService { get; private set; }

        private static HashSet<IService> _Services = new HashSet<IService>();

        /// <summary>
        /// Creates an instance of each service in a set order and adds it to an array. Then start the services in the order they're generated.
        /// </summary>
        /// <param name="taskQueue"></param>
        public static void Initialize()
        {
            _Services = null;
            _Services = new HashSet<IService>();

            BotService = GenerateService<BotService, BotServiceInterface>();
        }

        public static void StartServices()
        {
            foreach (IService service in _Services)
            {
                if (service.Dependencies != null)
                {
                    for (int x = 0; x < service.Dependencies.Length; x++)
                    {
                        if (service.Dependencies[x] is IService)
                        {
                            IService serviceDependency = service.Dependencies[x] as IService;
                            if (serviceDependency.IsServiceStarted == false)
                            {
                                serviceDependency.Start();
                                Debug.Log("[GlobalServicesProvider] Starting service dependency: " +
                                                       serviceDependency.GetType().Name);
                            }
                        }
                    }
                }
                service.Start();
            }
        }

        public static void LoadServices()
        {
            foreach (IService service in _Services)
            {
                service.LoadService();
            }
        }

        public static void UnloadServices()
        {
            foreach (IService service in _Services)
            {
                service.UnloadService();
            }
        }

        public static void StopServices()
        {
            foreach (IService service in _Services.Reverse())
            {
                service.Stop();
            }
        }

        public static void ReloadServices()
        {
            foreach (IService service in _Services)
            {
                service.ReloadService();
            }
        }

        public static IService[] GetServices()
        {
            return _Services.ToArray();
        }

        public static ServiceType GetService<ServiceType>(Type type) where ServiceType : class, IService
        {
            foreach (IService service in _Services)
            {
                if (service.GetType().IsAssignableFrom(type) || service.GetType().Equals(type))
                    return service as ServiceType;
            }

            return default(ServiceType);
        }

        private static ServiceType GenerateService<ServiceType, GUIRender>() where ServiceType : class, new()
        {
            ServiceType t = new ServiceType();
            _Services.Add(t as IService);
            return t;
        }

        private static ServiceType GenerateService<ServiceType>() where ServiceType : class, new()
        {
            return GenerateService<ServiceType, BaseServiceDebugWindow>();
        }

        public static string GenerateLogDump()
        {
            string dump = "";
            foreach (IService service in _Services.Reverse())
            {
                if (service == null)
                    continue;
                try
                {
                    dump += service.GetLogDump();
                } catch { }
            }
            return dump;
        }
    }
}