﻿using Boddle.Providers;
using System.Collections;
using UnityEngine;

namespace Boddle.App
{
    /// <summary>
    /// Replace with comments...
    /// </summary>
    public class App : MonoBehaviour
    {

        #region Properties
        public static App Instance;
        #endregion

        #region Fields

        #endregion

        #region Unity Methods
        private void Start()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
            GlobalDataProvider.Initialize();
            GlobalServicesProvider.Initialize();

            GlobalServicesProvider.StartServices();
            GlobalDataProvider.PreLoadData();

            GlobalServicesProvider.LoadServices();
            GlobalDataProvider.PostLoadData();

            StartCoroutine(Example());
        }

        IEnumerator Example()
        {
            yield return new WaitForSeconds(1);
            GameObject stateManager = new GameObject("[Core Game State Manager]");
        }

        private void OnGUI()
        {
            AppDebugGUI.RenderDebugGUI();
        }

        private void Update()
        {
        }
        #endregion

        #region Inaccessible Members

        #endregion

        #region Accessible Members

        #endregion

        #region Event Handlers

        #endregion
    }
}
