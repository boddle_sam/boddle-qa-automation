﻿using Boddle.Providers;
using Boddle.Services;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;

namespace Boddle.App
{
    /// <summary>
    /// Replace with comments...
    /// </summary>
    public abstract class DebuggableWindow<T> : IDebuggableGUIWindow where T : class
    {

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public void Reset()
        {
            val = null;
        }

        public Rect WindowRect => _rect;
        public int Id => _id;

        private Rect _rect;
        private int _id;
        private Vector2 _scroll;

        protected T Value
        {
            get
            {
                if (val == null)
                    val = GlobalServicesProvider.GetService<IService>(typeof(T)) as T;
                return val;
            }
        }
        private T val;

        public void OpenWindow(int id, Rect rect)
        {
            _rect = rect;
            _id = id;
        }
        public void CloseWindow()
        {
            AppDebugGUI.CloseWindow(this);
        }

        public void RenderGUI()
        {
            GUI.Box(WindowRect, "");
            _rect = GUI.Window(_id, WindowRect, (x) =>
            {
                RenderHeader();

                _scroll = GUILayout.BeginScrollView(_scroll);
                RenderGUI(x);
                GUILayout.EndScrollView();

                HandleDrag(); }, GetType().Name);
        }

        public abstract void RenderGUI(int id);

        public static Log RenderLogs<Log>(List<Log> logs, Log selectedLog, Action<Log> RenderAction, ref Vector2 scroll)
        {
            GUI.skin.button.alignment = TextAnchor.MiddleLeft;
            scroll = GUILayout.BeginScrollView(scroll);
            Log selected = selectedLog;
            for (int i = 0; i < logs.Count; i++)
            {
                if (GUILayout.Button(logs[i].ToString()))
                {
                    if (selected.Equals(logs[i]))
                        selected = default(Log);
                    else
                        selected = logs[i];
                }

                if (selected.Equals(logs[i]))
                {
                    RenderAction(logs[i]);
                }
            }
            GUILayout.EndScrollView();
            GUI.skin.button.alignment = TextAnchor.MiddleCenter;
            return selected;
        }


        private void RenderHeader()
        {
            if(GUI.Button(new Rect(WindowRect.width-30, 0, 22, 22), "X"))
            {
                CloseWindow();
            }
        }

        private void HandleDrag()
        {
            GUI.DragWindow();
        }
    }
}