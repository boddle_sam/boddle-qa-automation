﻿using UnityEngine;

namespace Boddle.App
{

    public interface IDebuggableGUIWindow
    {
        Rect WindowRect { get; }
        int Id { get; }

        void OpenWindow(int id, Rect rect);

        void CloseWindow();

        void RenderGUI();
    }

}