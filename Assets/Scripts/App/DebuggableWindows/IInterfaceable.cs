﻿using UnityEngine;

namespace Boddle.App
{
    /// <summary>
    /// Replace with comments...
    /// </summary>
    public interface IInterfaceable
    {
        IDebuggableGUIWindow DebuggableWindow { get; }
    }
}