﻿using Boddle.Providers;
using Boddle.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static Boddle.Services.BotService;

namespace Boddle.App
{
    /// <summary>
    /// Handles rendering the Debug GUI for the AppController and handles rendering of the Service/DataModel lists and every active window
    /// </summary>
    public static class AppDebugGUI
    {

        public const int LARGE_FONT_SIZE = 16;
        public const int SMALL_FONT_SIZE = 11;


        // Inaccessible Fields
        private static IService[] _servicesCache;

        private static HashSet<IDebuggableGUIWindow> _debuggableGUIWindows = new HashSet<IDebuggableGUIWindow>();

        private static Vector2 _serviceListScrollVector;

        private static bool _displayGameController;
        private static bool _displayServices;
        
        private static BotService BotService { get => GlobalServicesProvider.BotService; }

        public static void Initialize()
        {
            _debuggableGUIWindows = new HashSet<IDebuggableGUIWindow>();
            _servicesCache = null;
        }

        public static void Uninitialize()
        {
        }

        public static void CloseWindow(IDebuggableGUIWindow window)
        {
            _debuggableGUIWindows.Remove(window);
        }

        public static void RenderDebugGUI()
        {
            int prevFont = GUI.skin.label.fontSize;

            // Display a bot hud if it's available
            if(BotService.IsServiceLoaded)
            {
                BotServiceInterface debuggableGUIWindow = BotService.DebuggableWindow as BotServiceInterface;
                if(debuggableGUIWindow != null)
                    debuggableGUIWindow.DisplayBotHUD();
            }

            Rect serviceListRect = _displayServices ? new Rect(10, 10, 220, (Screen.height * 0.5f) - 20) : new Rect(10, 10, 220, 30);

            // Drag BG Boxes
            GUI.color = new Color(0.8f, 0.8f, 0.8f, 0.85f);
            GUI.Box(serviceListRect, "");
            GUI.Box(serviceListRect, "");
            GUI.color = Color.white;

            GUILayout.BeginArea(serviceListRect);
            GUI.skin.label.fontSize = LARGE_FONT_SIZE;
            GUILayout.BeginHorizontal();
            GUILayout.Label("Services");
            if (GUILayout.Button(_displayServices ? "-" : "+", GUILayout.Width(22)))
            {
                _displayServices = !_displayServices;
            }
            GUILayout.EndHorizontal();
            GUI.skin.label.fontSize = prevFont;
            if (_displayServices)
            {
                _serviceListScrollVector = GUILayout.BeginScrollView(_serviceListScrollVector);
                RenderServices();
                GUILayout.EndScrollView();
            }

            GUILayout.EndArea();
         
            RenderOpenedWindows();
        }

        private static void RenderServices()
        {
            if (_servicesCache == null)
                _servicesCache = GlobalServicesProvider.GetServices();

            foreach (IInterfaceable service in _servicesCache)
            {
                IDebuggableGUIWindow debuggableGUIWindow = service.DebuggableWindow;
                RenderListEntry(debuggableGUIWindow, service.GetType().Name);
            }
        }

        private static void RenderListEntry(IDebuggableGUIWindow debuggableGUIWindow, string entryName)
        {
            if (_debuggableGUIWindows.Contains(debuggableGUIWindow))
                GUI.color = Color.gray;

            if (GUILayout.Button(entryName))
            {
                OpenWindow(debuggableGUIWindow);
            }
            GUI.enabled = true;
            GUI.color = Color.white;
        }

        /// <summary>
        /// Adds the DebuggableGUIWindow to the list of Open Windows.
        /// </summary>
        /// <param name="debuggableGUIWindow"></param>
        private static void OpenWindow(IDebuggableGUIWindow debuggableGUIWindow)
        {
            int windowID = GetNewWinndowID();
            Rect windowRect = GetNewWindowRect();

            bool added = _debuggableGUIWindows.Add(debuggableGUIWindow);

            if (added)
            {
                debuggableGUIWindow.OpenWindow(windowID, windowRect);
            }
            else
            {
                GUI.BringWindowToFront(windowID);
            }
        }

        private static void RenderOpenedWindows()
        {
            foreach (IDebuggableGUIWindow debuggableGUIWindow in _debuggableGUIWindows)
            {
                debuggableGUIWindow.RenderGUI();
            }
        }

        /// <summary>
        /// Gets a new rect for a window with a fixed size and increases their position differences between each other as windows are added.
        /// </summary>
        /// <returns></returns>
        private static Rect GetNewWindowRect()
        {
            Rect contentViewRect = new Rect(240, 10, 600, 400);

            contentViewRect.x += _debuggableGUIWindows.Count * 10;
            contentViewRect.y += _debuggableGUIWindows.Count * 10;

            return contentViewRect;
        }

        /// <summary>
        /// Returns a totally unique ID for a window
        /// </summary>
        /// <returns></returns>
        private static int GetNewWinndowID()
        {
            int idGenerated = _debuggableGUIWindows.Count;

            while (_debuggableGUIWindows.Any(x => x.Id == idGenerated))
                idGenerated++;

            return idGenerated;
        }

        private static bool CanOpenDebugGUI()
        {
#if UNITY_EDITOR
            return true;
#endif
            if (Debug.isDebugBuild)
                return true;

            return true;
        }

    }
}
