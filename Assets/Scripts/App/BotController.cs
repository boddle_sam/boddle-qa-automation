﻿using Boddle.Data;
using Boddle.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Boddle.App
{
    public class BotController : MonoBehaviour
    {
        public void PlayBot(string ConfigName)
        {
            Debug.Log("[BotController] Playing bot [" + ConfigName + "]");
            BoddleBotConfigModel ConfigModel = GlobalDataProvider.BoddleBotsModel.BoddleBotConfigModels.FirstOrDefault(x => x.configName == ConfigName);
            GlobalServicesProvider.BotService.StartBot(ConfigModel);

        }
    }
}
