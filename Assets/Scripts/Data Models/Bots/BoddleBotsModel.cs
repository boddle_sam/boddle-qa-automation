﻿using System.Collections.Generic;
using UnityEngine;

namespace Boddle.Data
{

    /// <summary>
    /// 
    /// </summary>
    public class BoddleBotsModel : StaticDataModel<BoddleBotsModel>
    {

        public List<BoddleBotConfigModel> BoddleBotConfigModels;
        public BoddleBotModel[] BoddleBotModels;

        protected override void ApplyModel(BoddleBotsModel model)
        {
            BoddleBotConfigModels = model.BoddleBotConfigModels;
            BoddleBotModels = model.BoddleBotModels;
        }

    }
}
