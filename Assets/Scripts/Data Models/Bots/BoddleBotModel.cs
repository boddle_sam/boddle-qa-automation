﻿using System.Collections.Generic;
using Unity.AutomatedQA;
using UnityEngine;

namespace Boddle.Data
{

    /// <summary>
    /// 
    /// </summary>
    public class BoddleBotModel : StaticDataModel<BoddleBotModel>
    {
        [SerializeReference]
        public List<AutomatorConfig> automators = new List<AutomatorConfig>();

        protected override void ApplyModel(BoddleBotModel model)
        {
            automators = this.automators;
        }

    }
}
