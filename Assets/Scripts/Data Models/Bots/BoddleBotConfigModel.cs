﻿using UnityEngine;

namespace Boddle.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class BoddleBotConfigModel : StaticDataModel<BoddleBotConfigModel>
    {

        [Tooltip("The bot we want to execute.")]
        public BoddleBotModel BotToRun;

        [Tooltip("An identifier for this configuration.")]
        public string configName;

        [Tooltip("Amount of times to repeat this bot run. Enter -1 to repeat forever and periodically report its status.")]
        public int timesToRepeat;

        [Tooltip("Toggle screenshot capture on and off for performance considerations.")]
        public bool captureScreenshots;

        [Tooltip("Amount of logs to collect once the bot is ran. Enter -1 to store all logs.")]
        public int maxLogCount;

        [Tooltip("Should we report the results when the test is completed?")]
        public bool reportResults;

        [Tooltip("Should the game automatically quit when the test is completed?")]
        public bool quitOnComplete;

        protected override void ApplyModel(BoddleBotConfigModel model)
        {
            BotToRun = model.BotToRun;
            configName = model.configName;
            timesToRepeat = model.timesToRepeat;
            captureScreenshots = model.captureScreenshots;
            maxLogCount = model.maxLogCount;
            reportResults = model.reportResults;
            quitOnComplete = model.quitOnComplete;
        }

    }
}
