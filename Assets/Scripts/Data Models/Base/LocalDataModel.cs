﻿using Boddle.App;
using Boddle.Utils;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Boddle.Data
{
    /// <summary>
    /// Allows for the saving and loading of the data locally on the user's device
    /// </summary>
    /// <typeparam name="DataModelType"></typeparam>
    public abstract class LocalDataModel<DataModelType> : DataModel<DataModelType> where DataModelType : DataModel<DataModelType> { }
    public abstract class LocalDataModel<DataModelType, GUIRender> : DataModel<DataModelType>, IDataModel where DataModelType : DataModel<DataModelType> where GUIRender : class, new()
    {

        public override LoadType LoadType => LoadType.PreLoad;

        public override async void LoadModel()
        {
        }

    }
}