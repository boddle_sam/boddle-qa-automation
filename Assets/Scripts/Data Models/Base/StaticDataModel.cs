﻿using Boddle.App;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Boddle.Data
{
	/// <summary>
	/// Used for game data that isn't modified anywhere but through the editor.
	/// </summary>
	/// <typeparam name="DataModelType"></typeparam>
	public abstract class StaticDataModel<DataModelType> : DataModel<DataModelType> where DataModelType : DataModel<DataModelType> 
	{

		public override LoadType LoadType => LoadType.PreLoad;

		public override async void LoadModel()
		{
			_isLoaded = true;
		}

		public override void UnloadModel()
		{
			_isLoaded = false;
		}

		protected abstract void ApplyModel(DataModelType model);

	}
}