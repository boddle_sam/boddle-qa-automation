﻿using System.Runtime.InteropServices;
using UnityEditor;
using UnityEngine;

namespace Boddle.Utils
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="ScriptableObjT"></typeparam>
    public abstract class InstancedScriptableObject<ScriptableObjT> : ScriptableObject where ScriptableObjT : ScriptableObject
    {
    }
}
