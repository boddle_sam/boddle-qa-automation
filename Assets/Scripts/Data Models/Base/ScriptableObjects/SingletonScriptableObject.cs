﻿using Boddle.Services;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Boddle.Utils
{
    /// <summary>
    /// Intended to act as a singleton scriptable object accessed in run-time and from editor.
    /// </summary>
    /// <typeparam name="ScriptableObjT"></typeparam>
    public abstract class SingletonScriptableObject<ScriptableObjT> : ScriptableObject where ScriptableObjT : ScriptableObject
    {
        public const string resourcesPath = "Assets/@@BoddleGame/Data/Resources/";

        public static ScriptableObjT Instance
        {
            get
            {
                if (_instance == null)
                {
                    try
                    {
                        string path = GetScriptableObjectName();
                        _instance = Resources.Load(path, typeof(ScriptableObjT)) as ScriptableObjT;
                    }
                    catch
                    {
                    }
                }

                return _instance;
            }
        }
        private static ScriptableObjT _instance;

        public static void CreateMyAsset()
        {
#if UNITY_EDITOR
            if (Instance != null)
            {
                return;
            }
            ScriptableObjT asset = ScriptableObject.CreateInstance<ScriptableObjT>();
            AssetDatabase.CreateAsset(asset, resourcesPath + GetScriptableObjectName() + ".asset");
            AssetDatabase.SaveAssets();
#endif
        }

        public static string GetScriptableObjectName()
        {
            return typeof(ScriptableObjT).Name.ToString();
        }
    }
}
