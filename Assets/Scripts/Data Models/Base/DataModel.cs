﻿using Boddle.App;
using Boddle.Providers;
using Boddle.Services;
using Boddle.Utils;
using System.Text;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Boddle.Data
{

    public enum SerializationMethods
    {
        JSON = 0
    };

    public abstract class DataModel<DataModelType> : InstancedScriptableObject<DataModelType>, IDataModel where DataModelType : ScriptableObject, IDataModel
    {

        public bool IsLoaded { get => _isLoaded; }
        public abstract LoadType LoadType { get; }

        protected bool _isLoaded;

        public virtual void InitializeModel() 
        {
            _isLoaded = false;
        }
        public abstract void LoadModel();       

        public void MarkAsLoaded()
        {
            _isLoaded = true;
        }

        public virtual void UnloadModel()
        {
            _isLoaded = false;
        }


        public virtual string GetLogDump()
        {
            string dump = "";
            dump += "***************************************" + "\n";
            dump += "[DATA MODEL-" + typeof(DataModelType).Name + "]" + "\n";
            dump += "***************************************" + "\n";
            dump += "Is Loaded: " + _isLoaded + "\n";
            dump += "\n\n";
            return dump;
        }
    }
}

