﻿using Boddle.App;
using Boddle.Services;
using System.Threading.Tasks;

namespace Boddle.Data
{

    public enum LoadType { PreLoad, PostLoad, PreAuthenticate, PostAuthenticate, Manual };

    public interface IDataModel
    {
        bool IsLoaded { get; }
        LoadType LoadType { get; }

        void InitializeModel();
        void LoadModel();
        void MarkAsLoaded();
        void UnloadModel();

        string GetLogDump();
    }
}