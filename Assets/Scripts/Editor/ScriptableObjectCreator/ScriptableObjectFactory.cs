﻿using Boddle.App;
using Boddle.Data;
using Boddle.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Helper class for instantiating ScriptableObjects.
/// </summary>
public class ScriptableObjectFactory
{
	[MenuItem("Tools/Scriptable Object Creator/Generic")]
	[MenuItem("Assets/Create/ScriptableObject")]
	public static void CreateGeneric()
	{
		var assembly = GetAssembly();

		// Get all classes derived from ScriptableObject
		var allScriptableObjects = (from t in assembly.GetTypes()
									where t.IsSubclassOf(typeof(ScriptableObject))
									select t).ToArray();

		// Show the selection window.
		var window = EditorWindow.GetWindow<ScriptableObjectWindow>(true, "Create a new ScriptableObject", true);
		window.ShowPopup();

		window.Types = allScriptableObjects;
	}

	[MenuItem("Tools/Scriptable Object Creator/Boddle Data Model")]
	public static void CreateBoddleInstance()
	{
		var assembly = GetAssembly();

		// Get all classes derived from ScriptableObject
		List<Type> allScriptableObjects = (from t in assembly.GetTypes()
										   select t).ToList();

		allScriptableObjects.Remove(typeof(InstancedScriptableObject<>));
		allScriptableObjects.Remove(typeof(DataModel<>));
		allScriptableObjects.Remove(typeof(StaticDataModel<>));
		allScriptableObjects.Remove(typeof(LocalDataModel<,>));
		allScriptableObjects.Remove(typeof(LocalDataModel<>));

		// Show the selection window.
		ScriptableObjectWindow window = EditorWindow.GetWindow<ScriptableObjectWindow>(true, "Create a new Boddle Data Model", true);
		window.ShowPopup();

		window.Types = allScriptableObjects.ToArray();
	}

	/// <summary>
	/// Returns the assembly that contains the script code for this project (currently hard coded)
	/// </summary>
	private static Assembly GetAssembly ()
	{
		return Assembly.Load (new AssemblyName ("Assembly-CSharp"));
	}
}