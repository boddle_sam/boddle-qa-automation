﻿using Boddle.App;
using Boddle.Services;
using System;
using UnityEngine;
using System.Collections.Generic;

namespace Boddle.Services
{
    public abstract class Service<ServiceType> : Service<ServiceType, BaseServiceDebugWindow> where ServiceType : class { }

    /// <summary>
    /// Replace with comments...
    /// </summary>
    public abstract class Service<ServiceType, GUIRender> : IService, IInterfaceable where ServiceType : class where GUIRender : class, new()
    {
        #region Properties
        public virtual Type[] Dependencies { get; }
        public IDebuggableGUIWindow DebuggableWindow
        {
            get
            {
                if (_debuggableWindow == null)
                    _debuggableWindow = new GUIRender();
                return _debuggableWindow as IDebuggableGUIWindow;
            }
        }
        public bool IsServiceStarted { get => _startedService; }
        public bool IsServiceLoaded { get => _loadedService; }
        #endregion

        #region Fields
        private GUIRender _debuggableWindow;
        private bool _startedService;
        private bool _loadedService = false;
        private List<string> _logs = new List<string>();
        private int _errors;
        #endregion

        #region Inaccessible Members
        protected virtual void Log(string message)
        {
            _logs.Add(message);
            if (_logs.Count >= 50)
                _logs.RemoveAt(0);
            Debug.Log("<b>[Service] [" + typeof(ServiceType).Name.ToString() + "]</b> " + message);
        }
        protected virtual void LogError(string message)
        {
            _logs.Add("Error! " + message);
            _errors++;
            if (_logs.Count >= 50)
                _logs.RemoveAt(0);
            Debug.LogError("<b>[Service] [" + typeof(ServiceType).Name.ToString() + "]</b> " + message);
        }
        #endregion

        #region Accessible Members
        public void Start() 
        {
            _loadedService = false;
            _startedService = true;
            _logs = new List<string>();
            StartService();
        }
        public abstract void StartService();
        public virtual void LoadService() { _loadedService = true; }
        public virtual void UnloadService() { _loadedService = false; }
        public abstract void ReloadService();
        public void Stop()
        {
            _startedService = false;
            StopService();
        }

        public abstract void StopService();

        #endregion

        public virtual string GetLogDump()
        {
            string dump = "";
            dump += "***************************************" + "\n";
            dump += "[SERVICE-" + typeof(ServiceType).Name + "]" + "\n";
            dump += "***************************************" + "\n";
            dump += "Errors: " + _errors + "\n";
            dump += "\nLog Start (Last 50 Logs)" + "\n";
            dump += "--------------" + "\n";
            _logs.ForEach(x => dump += x.ToString() + "\n");
            dump += "--------------" + "\n";
            dump += "Log End" + "\n";
            dump += "\n\n";
            return dump;
        }

        #region Event Handlers

        #endregion
    }
}