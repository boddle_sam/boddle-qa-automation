﻿using Boddle.App;
using System;
using UnityEngine;

namespace Boddle.Services
{
    /// <summary>
    /// Replace with comments...
    /// </summary>
    public interface IService
    {

        Type[] Dependencies { get; }

        bool IsServiceStarted { get; }

        void Start();
        void LoadService();
        void UnloadService();
        void ReloadService();
        void Stop();

        string GetLogDump();

    }
}