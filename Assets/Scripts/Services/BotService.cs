﻿using Boddle.App;
using Boddle.Data;
using Boddle.Providers;
using System;
using Unity.AutomatedQA;
using Unity.RecordedPlayback;
using UnityEngine;
using UnityEngine.EventSystems;
using static Boddle.Services.BotService;
using static Unity.AutomatedQA.AutomatedRun;

namespace Boddle.Services
{

    /// <summary>
    /// Keeps track of whether the game is in the background or not. On WebGL it will check if its in another tab.
    /// </summary>
    public partial class BotService : Service<BotService, BotServiceInterface>
	{
		public override Type[] Dependencies
		{
			get => new Type[]
			{
			};
		}

		#region Properties
		#endregion

		#region Fields
		private BoddleBotConfigModel m_activeBot;
		private bool m_isRunning;
		private bool m_startedAppRunning;
		private AQALogger m_logger;

		// Used to control the bot as a monobehavior
		private BotController m_botController;
		private RecordingInputModule m_botControllerRecordingInput;
		#endregion

		#region Inaccessible Members
		#endregion

		#region Accessible Members

		public override void StartService()
		{
			if (m_botController == null)
			{
				GameObject botControllerObj = new GameObject("[BotController]");
				GameObject.DontDestroyOnLoad(botControllerObj);
				m_botController = botControllerObj.AddComponent<BotController>();
			}

			// Check if we're starting the service while a report is in progress.
			if (String.IsNullOrEmpty(ReportingManager.CurrentTestName))
			{
				m_startedAppRunning = false;
			}
            else
            {
				m_startedAppRunning = true;
			}
		}

		public override void ReloadService() 
		{ 
		}

		public override void StopService()
		{
			GameObject.Destroy(m_botController.gameObject);
		}

		public void StartBot(BoddleBotConfigModel botTestsModel)
        {
			Log("Starting bot: " + botTestsModel.configName);
			m_logger = new AQALogger();

			// Create a new run config and direct the automation controller to run the specified automators
			RunConfig runConfig = new RunConfig()
			{
				quitOnFinish = false, // Set to false to override the behavior of quitting 
				automators = botTestsModel.BotToRun.automators
			};
			m_activeBot = botTestsModel;
			m_isRunning = true;

			if (m_startedAppRunning == false)
			{
				// Try to get the recording input module
				if (m_botControllerRecordingInput == null)
					m_botControllerRecordingInput = RecordingInputModule.Instance;
				if (m_botControllerRecordingInput == null)
					m_botControllerRecordingInput = EventSystem.current.GetComponent<RecordingInputModule>();
				if (m_botControllerRecordingInput == null)
					m_botControllerRecordingInput = EventSystem.current.gameObject.AddComponent<RecordingInputModule>();
				RecordingInputModule.Instance = m_botControllerRecordingInput;

				// Setup the recording playback
				RecordedPlaybackController.Instance.Reset();

                Driver.Reset();

				if (ReportingMonitor.Instance != null)
				{
					ReportingMonitor.Destroy(ReportingMonitor.Instance);
				}
				ReportingManager.Reset();

				ReportingManager.CurrentTestName = botTestsModel.configName;
				ReportingManager.IsTestWithoutRecordingFile = true;
				ReportingManager.IsCrawler = true;
				ReportingManager.CreateMonitoringService();

				VisualFxManager.SetUp(m_botController.transform);
			}

			CentralAutomationController.Instance.ResetAutomators();
			CentralAutomationController.Instance.Run(runConfig);
		}

		public void CompleteBot(BoddleBotConfigModel botTestsModel)
        {
			Log("Completing bot: " + botTestsModel.configName);
			ReportingManager.CurrentTestName = "";
			m_activeBot = null;
			m_isRunning = false;
		}

		#endregion
	}
}