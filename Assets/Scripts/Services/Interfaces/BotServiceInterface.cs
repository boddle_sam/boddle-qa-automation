﻿using Boddle.App;
using Boddle.Data;
using Boddle.Providers;
using System.Collections.Generic;
using Unity.AutomatedQA;
using UnityEngine;
using static Boddle.Services.BotService;

namespace Boddle.Services
{
	/// <summary>
	/// Replace with comments...
	/// </summary>
	/// 
	// Use partial class to access the member values on BotService
	public partial class BotService : Service<BotService, BotServiceInterface>
	{
		public class BotServiceInterface : DebuggableWindow<BotService>
		{

			private static BoddleBotsModel BoddleBotsModel { get => GlobalDataProvider.BoddleBotsModel; }

			private Vector2 m_configListScrollPos;
			private Vector2 m_automatorsListScrollPos;
			private Vector2 m_configScrollPos;
			private BoddleBotConfigModel m_selectedBotConfig;
			private bool m_selectingBot = false;

			public override void RenderGUI(int id)
			{
				if (m_selectedBotConfig == null && BoddleBotsModel.BoddleBotConfigModels.Count > 0)
					m_selectedBotConfig = BoddleBotsModel.BoddleBotConfigModels[0];

				if (Value.m_isRunning)
					RenderActiveBot();
				else
					RenderBotSelection();
			}

			public void RenderBotSelection()
			{
				// ****************************************************
				// Render configuration selection rect
				// ****************************************************
				Rect configSelectionRect = new Rect(5, 5, 200, WindowRect.height - 40);
				GUI.Box(configSelectionRect, "");
				GUILayout.BeginArea(configSelectionRect);

				GUILayout.Box("Boddle Bot Configs", GUILayout.Height(25f));

				m_configListScrollPos = GUILayout.BeginScrollView(m_configListScrollPos);

				for (int i = 0; i < BoddleBotsModel.BoddleBotConfigModels.Count; i++)
				{
					if (m_selectedBotConfig == BoddleBotsModel.BoddleBotConfigModels[i])
						GUI.color = Color.gray;
					else
						GUI.color = Color.white;

					if (GUILayout.Button(BoddleBotsModel.BoddleBotConfigModels[i].configName))
					{
						m_selectedBotConfig = BoddleBotsModel.BoddleBotConfigModels[i];
						m_selectingBot = false;
					}
				}
				GUI.color = Color.white;

				GUILayout.Space(10);
				if (GUILayout.Button("Create new config"))
				{
					m_selectedBotConfig = GenerateNewConfig();
				}

				GUILayout.EndScrollView();
				GUILayout.EndArea();

				// ****************************************************
				// Render selected bot configuration 
				// ****************************************************
				Rect configRect = new Rect(configSelectionRect.width + 10, 5, WindowRect.width - configSelectionRect.width - 35, WindowRect.height - 80);
				GUI.Box(configRect, "");
				GUILayout.BeginArea(configRect);
				m_configScrollPos = GUILayout.BeginScrollView(m_configScrollPos);

				if(m_selectedBotConfig != null)
				{
					m_selectedBotConfig.configName = GUILayout.TextField(m_selectedBotConfig.configName);

					if (m_selectedBotConfig.BotToRun == null)
						m_selectedBotConfig.BotToRun = BoddleBotsModel.BoddleBotModels[0];

					GUILayout.BeginHorizontal();
					GUILayout.Label("Bot to Play: ");
					if(GUILayout.Button(m_selectedBotConfig.BotToRun.name + ", " + m_selectedBotConfig.BotToRun.automators.Count + " steps", GUILayout.Width(250)))
                    {
						m_selectingBot = !m_selectingBot;
                    }
					GUILayout.EndHorizontal();

					// Display drop down to select a bot to use
					if (m_selectingBot)
					{
						for (int i = 0; i < BoddleBotsModel.BoddleBotModels.Length; i++)
						{
							string name = BoddleBotsModel.BoddleBotModels[i].name;

							if (BoddleBotsModel.BoddleBotModels[i] == m_selectedBotConfig.BotToRun)
								GUI.color = Color.gray;
							else
								GUI.color = Color.white;

							if (GUILayout.Button(name))
							{
								m_selectedBotConfig.BotToRun = BoddleBotsModel.BoddleBotModels[i];
							}
						}
					}

					GUILayout.BeginHorizontal();
					GUILayout.Label("Times to Repeat: ");
					try
					{
						m_selectedBotConfig.timesToRepeat = int.Parse(GUILayout.TextField(m_selectedBotConfig.timesToRepeat.ToString(), GUILayout.Width(50)));
					}
					catch { }
					GUILayout.EndHorizontal();

					GUILayout.BeginHorizontal();
					GUILayout.Label("Capture Screenshots: ");
					m_selectedBotConfig.captureScreenshots = GUILayout.Toggle(m_selectedBotConfig.captureScreenshots, "", GUILayout.Width(50));
					GUILayout.EndHorizontal();


				}
				else
                {
					GUILayout.Label("No bot selected");
                }

				GUILayout.EndScrollView();
				GUILayout.EndArea();
				
				if (GUI.Button(new Rect(configRect.x + configRect.width - 75, configRect.height + 10, 75, 30), "Start"))
				{
					StartBotInFrame();
                }
			}

			void StartBotInFrame()
            {
				Value.StartBot(m_selectedBotConfig);
			}

			public void RenderActiveBot()
			{
				List<Automator> automators = CentralAutomationController.Instance.GetAllAutomators();
				List<AutomatorConfig> automatorConfigs = m_selectedBotConfig.BotToRun.automators;

				// ****************************************************
				// Render the steps of the bot
				// ****************************************************
				Rect configSelectionRect = new Rect(5, 5, 200, WindowRect.height - 40);
				GUI.Box(configSelectionRect, "");
				GUILayout.BeginArea(configSelectionRect);

				GUILayout.Box("Automators", GUILayout.Height(25f));

				m_automatorsListScrollPos = GUILayout.BeginScrollView(m_automatorsListScrollPos);

				for (int i = 0; i < automators.Count; i++)
				{
					GUILayout.Label(automators[i].name + " - " + automators[i].state);
				}

				for (int i = 0; i < automatorConfigs.Count; i++)
				{
					GUILayout.Label(automatorConfigs[i].ToString() + " - " + automatorConfigs[i].AutomatorType);
				}

				GUILayout.EndScrollView();
				GUILayout.EndArea();

				// ****************************************************
				// Render the metrics of the bot and info on the configuration
				// ****************************************************
				Rect configRect = new Rect(configSelectionRect.width + 10, 5, WindowRect.width - configSelectionRect.width - 35, WindowRect.height - 80);
				GUI.Box(configRect, "");
				GUILayout.BeginArea(configRect);

				m_configListScrollPos = GUILayout.BeginScrollView(m_configListScrollPos);

				GUILayout.EndScrollView();
				GUILayout.EndArea();

				if (GUI.Button(new Rect(configRect.x + configRect.width - 75, configRect.height + 10, 75, 30), "Pause"))
				{
				}
			}

			public void DisplayBotHUD()
			{
				if (Value.m_isRunning)
				{
					GUILayout.BeginArea(new Rect((Screen.width * 0.5f) - 150, Screen.height - 400, 300, 400));

					GUILayout.Label("Running Boddle");
					GUILayout.Label("Config - " + Value.m_activeBot.name);
					GUILayout.Label("Bot - " + Value.m_activeBot.BotToRun.name);

					GUILayout.EndArea();
				}
			}

			private BoddleBotConfigModel GenerateNewConfig()
            {
				BoddleBotConfigModel newModel = BoddleBotConfigModel.CreateInstance<BoddleBotConfigModel>();
				newModel.configName = "New Bot Config";
				newModel.name = newModel.configName;
				return newModel;
			}
		}
	}
}